//Visualizar la barra de herramientas cuando haga click en el menu de header
$(".Header__menu").click(function(){
  $(".toolbar").toggleClass("--visible");
  $(this).toggleClass("--active");
});
//Ocultar la barra de herramientas cuando haga click en el menu de header
$(".toolbar__menu").click(function(){
  $(".toolbar").removeClass("--visible");
  $(".Header__menu").removeClass("--active");
});
//Visualizar la herramienta seleccionada a partir del ID que se encuentra dentro del boton del menu principal
$(".toolbar__button#toolbar__button__filter").click(function(){
  $(".filter").addClass("--visible");
});
$(".toolbar__button#toolbar__button__about").click(function(){
  $(".about").addClass("--visible");
});
$(".toolbar__button#toolbar__button__layers").click(function(){
  $(".layers").addClass("--visible");
});
$(".toolbar__button#toolbar__button__basemap").click(function(){
  $(".basemap").addClass("--visible");
});
$(".toolbar__button#toolbar__button__accessibility").click(function(){
  $(".accessibility").addClass("--visible");
});
//Ocultar la barra de herramientas cuando haga click en el menu de header
$(".toolbar__secondPanelTop__menu").click(function(){
  $(".toolbar__function").removeClass("--visible");
});

