//Descripcion - Este js es para traer los datos de la divipola de los filtros geograficos del visor por departamentos, el servicio geografico es el que tiene la lista de los departamentos
var $var = require("../global/variables");//Variables - importar variables globales
var dptoArray = require("../../json/departamentos.json");//json con array de dpto

//loadDivipola - Funcion para traer los departamentos y llenarlos en una lista
$var.Map.loadDivipola = function () {
	console.log(dptoArray)
	// $.ajax({
	// 	url: dptoArray,
	// 	type: "GET",
	// 	success: function (data) {
	$("#filter__departamento .filter__selectBox__list").html("") // Filtro - aqui se limpia la lista donde coloca lis items de la divipola
	$('<li class="filter__selectBox__list__item" value="00"><p class="filter__selectBox__list__item__text"><span class="filter__selectBox__list__item__text__code">00</span> - Todos los departamentos</p></li>').appendTo($("#filter__departamento .filter__selectBox__list"))// Filtro - coloca el valor por defecto
	// if(data.resultado.length > 0){				
	// 	for(var i = 0; i < data.resultado.length; i++){ //Lista- Aqui se llena la lista del select de departamento con los departamentos 
	dptoArray.map(function (obj, j, k) {
		$('<li class="filter__selectBox__list__item liDpto" value="' + obj.cod_dpto + '"><p class="filter__selectBox__list__item__text"><span class="filter__selectBox__list__item__text__code">' + obj.cod_dpto + '</span> - ' + obj.nombre.toUpperCase() + '</p></li>').appendTo($("#filter__departamento .filter__selectBox__list"))// Filtro - Aqui se pintan los departamentos en html en el select
		$var.Map.listaDivipolaDeptos.push(obj.cod_dpto, obj.nombre.toUpperCase()) // Lista - Aqui se llena el arreglo con la lista de departamentos existentes
	}, []);
	// 	}        
	// }
	$(".filter__selectBox__list__item").first().addClass("--highlight"); // Item - Se asigna la clase para resaltar el primer depto de la lista
	//Filtro Accion - Aqui es cuando se hace la acción de click en el item del departamento
	$(".filter__selectBox__list__item").on("click", function (e) {
		// console.log($(this).val());
		e.stopPropagation(); //Click - para evitar que otras funcionalidades afecten al click hecho sobre el item
		$('#filter__departamento .filter__selectBox__list').removeClass("--active");//Click - para quitar la visualizacion de la lista de deparamentos
		$('#filter__municipio').removeClass("--invisible");//Click - para visualizar el filtro de municipios se quita la clase --invisible			
		$('#filter__depto').val($(this).text()); // Texto - para que en el input salga el valor del departamento seleccionado
		$var.Map.deptovalue = $(this).find(".filter__selectBox__list__item__text__code").text(); // Value - Para asignar le nuevo valor del departamento seleccionado
		$("#filter__mpio").text(" ")
		if ($var.Map.deptovalue == "00") {//Validar - Si el valor 
			$("#filter__depto").text("00 - Todos los departamentos") //Filtro - asignar valor al input de todos los departamentos					
			$('#filter__municipio').addClass("--invisible");
			$("#filter__municipio .filter__selectBox__list").html("");
			$var.Map.listaDivipolaMpios = []
		} else {
			$var.Map.loadMunicipios(); //Ir a la funcion para cargar los municipios - loadmpios.js
		}
	});
	// 	}
	// }) 
}