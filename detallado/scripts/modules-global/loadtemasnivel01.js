var $var = require("../global/variables");//importar variables globales
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
$var.Map.loadtemasnivel01 = function(){
    var servicioTemas = $var.Map.urlTemas + '?codigo=' + $var.Map.codigoVisor;
    $.ajax({
        url: servicioTemas,
        type: "GET",
        success: function (data) {
            if (data.length > 0) {  
                var firstTematica = false;
                var listaTipoTemas = [];
                for (var i = 0; i < data.length; i++) {
                    listaTipoTemas.push(data[i]["TIPO_TEMATICA"])
                }
                listaTipoTemas = $var.Map.unique(listaTipoTemas);//Para sacar los temas unicos dentro de ese array y no se repitan              
                if (listaTipoTemas.length == 0) {                    
                    for (var i = 0; i < data.length; i++) {
                        if (!firstTematica) {
                            $('<li class="filter__tematicas__selectList__item --tema'+ data[i]["CLASE_COLOR"] + '" id="' + data[i]["COD_GRUPO"] + '"><div class="filter__tematicas__selectList__item__btn --nameTema --active"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><div class="filter__tematicas__selectList__item__btn__icon"><span class="' + data[i]["CLASE_ICONO"] + '"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["GRUPO"] + '</p></div><ul class="filter__tematicas__selectList__item__subtemasList"></ul></li>').appendTo($(".filter__tematicas__list"));
                            firstTematica = true;
                        }
                        else {
                            $('<li class="filter__tematicas__selectList__item --tema ' + data[i]["CLASE_COLOR"] + '" id="' + data[i]["COD_GRUPO"] + '"><div class="filter__tematicas__selectList__item__btn --nameTema"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><div class="filter__tematicas__selectList__item__btn__icon"><span class="' + data[i]["CLASE_ICONO"] + '"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["GRUPO"] + '</p><ul class="filter__tematicas__selectList__item__subtemasList"></ul></div></li>').appendTo($(".filter__tematicas__list"));
                        }
                    }
                }
                else {                    
                    $(".filter__tematicas").html("");
                    //Para pintar los tipos de temas a los que pertenecen (temas generales/Enfoque de genero/indicadores sociodemograficos) estos estan difinidos en la funcion $var.Map.getTipoTematica - Ubicacion vend-local getTipoTematica.js
                    for (var i = 0; i < listaTipoTemas.length; i++) {
                        if (i == 0) {
                            $('<button class="filter__tematicas__typebtn --active"><p class="filter__tematicas__typebtn__name">' + $var.Map.getTipoTematica(listaTipoTemas[i]) + '</p></button><div class="filter__subtemas__container --open"><ul class="filter__tematicas__selectList --tipoTema' + listaTipoTemas[i] + ' --tematicas"></ul></div>').appendTo($(".filter__tematicas"));
                        }
                        else {
                            $('<button class="filter__tematicas__typebtn"><p class="filter__tematicas__typebtn__name">' + $var.Map.getTipoTematica(listaTipoTemas[i]) + '</p></button><div class="filter__subtemas__container"><ul class="filter__tematicas__selectList --tipoTema' + listaTipoTemas[i] + ' --tematicas"></ul></div>').appendTo($(".filter__tematicas"));
                        }
                    }
                    for (var i = 0; i < data.length; i++) {
                        //aqui se activa la primera tematica
                        if (!firstTematica) {
                            $('<li class="filter__tematicas__selectList__item --tema" id="' + data[i]["COD_GRUPO"] + '"><div class="filter__tematicas__selectList__item__btn --nameTema --active"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><div class="filter__tematicas__selectList__item__btn__icon ' + data[i]["CLASE_COLOR"] + '"><span class="' + data[i]["CLASE_ICONO"] + '"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["GRUPO"] + '</p></div><ul class="filter__tematicas__selectList__item__subtemasList --active"></ul></li>').appendTo($(".--tipoTema" + data[i]["TIPO_TEMATICA"]));
                            firstTematica = true;
                        }
                        else {
                            $('<li class="filter__tematicas__selectList__item --tema" id="' + data[i]["COD_GRUPO"] + '"><div class="filter__tematicas__selectList__item__btn --nameTema"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><div class="filter__tematicas__selectList__item__btn__icon ' + data[i]["CLASE_COLOR"] + '"><span class="' + data[i]["CLASE_ICONO"] + '"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["GRUPO"] + '</p></div><ul class="filter__tematicas__selectList__item__subtemasList"></ul></li>').appendTo($(".--tipoTema" + data[i]["TIPO_TEMATICA"]));
                        }
                    }
                }
            }
        }
    });
}