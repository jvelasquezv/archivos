//Descripcion - Este js es para traer los datos de los municipios del departamento seleccionado en loaddivipola,js que se encuentra en los filtros geograficos del visor, el servicio geografico es el que tiene la lista de los municipios
var $var = require("../global/variables");//Variables - importar variables globales
var mpioArray = require("../../json/municipios.json");//json con array de dpto
var bbox = require("../../json/bbox.json");//json con array de dpto

//loadMunicipios - Funcion para traer los municipios y llenarlos en una lista
$var.Map.loadMunicipios = function () {
    console.log('Valor de depto que entro a loadmunicipios', $var.Map.deptovalue)
    // $.ajax({
    //     url: $var.Map.urlDivipola + "?cod_depto=" + $var.Map.deptovalue,//URL - A partir del valor seleccionado en loaddivipola, se asigno a deptovalue el valor del departamento y pintar sus municipios
    //     type: "GET",
    //     success: function (data) {
    $var.Map.listaDivipolaMpios = [] //Arreglo - Se vacia el arreglo con la lista de los municipios
    $("#filter__municipio .filter__selectBox__list").html("");//Lista - Se vacia la lista de los municipios
    $("#filter__mpio").val("00000 - Todos los municipios")//Lista - Se asigna el valor al input de todos por defecto
    // if(data.resultado.length > 0){                
    //     for (var i = 0; i < data.resultado.length; i++) {
    let mpioByDpto = groupByFunct(mpioArray, "cod_dpto");
    if (mpioByDpto[$var.Map.deptovalue] != undefined) {
        mpioByDpto[$var.Map.deptovalue].map(function (obj, j, k) {
            $('<li class="filter__selectBox__list__item" value="' + obj.cod_mpio + '"><p class="filter__selectBox__list__item__text"><span class="filter__selectBox__list__item__text__code">' + obj.cod_mpio + '</span> - ' + obj.nombre.toUpperCase() + '</p></li>').appendTo($("#filter__municipio .filter__selectBox__list"));
            $var.Map.listaDivipolaMpios.push([obj.cod_mpio, obj.nombre])
        }, []);
    }
    //     }       
    // }
    //Filtros de departamento cuando haga click y asigne el valor 
    $("#filter__municipio .filter__selectBox__list__item").on("click", function (e) {
        e.stopPropagation();
        $('#filter__municipio .filter__selectBox__list').removeClass("--active");
        $('#filter__municipio').removeClass("--invisible");
        $('#filter__mpio').val($(this).text());
        filterGeograficoMpio($(this).text())
        $var.Map.mpiovalue = $(this).find(".filter__selectBox__list__item__text__code").text();
    });
    //     }		
    // })
    function filterGeograficoMpio(mecha) {
        mecha = mecha.split("-");
        console.log(mecha[0].trim());
        cargarInformacion(mecha[0].trim());
        let bboxTemp = bbox[mecha[0].trim()];
        $var.Map.map.fitBounds([
            [bboxTemp[0][1], bboxTemp[0][0]],
            [bboxTemp[1][1], bboxTemp[1][0]]
        ]);
    }

    //* Función - AGRUPAR POR CAMPO
    function groupByFunct(array, key) {
        const groupBy = array.reduce((objectsByKeyValue, obj) => {
            const value = obj[key];
            objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
            return objectsByKeyValue;
        }, {});
        return groupBy
    }

    function cargarInformacion(mpio) {
        const dataFiltro = $var.Map.listaVariables[$var.Map.varVariable][0];
        let urlData = "", campoLabel = "";
        // arrayVariable[data[i]["COD_CATEGORIA"]] = [data[i]["CAMPO_TABLA"].toLowerCase(), data[i]["TIPO_VARIABLE"], data[i]["CATEGORIA"]];
        // listaVariables.reduce((id, thing) => {
        //     if (thing[0] == varVariable) {
        //         campoLabel = thing[1];
        //     }
        // }, []);
        if ($var.Map.listaVariables[$var.Map.varVariable][1] == "VA") {
            campoLabel = "% " + $var.Map.listaVariables[$var.Map.varVariable][2];
        } else {
            campoLabel = "Conteo " + $var.Map.listaVariables[$var.Map.varVariable][2];
        }
        if (dataFiltro == 'ipm') {
            valorCampo = 'IPM'
            urlData = 'http://geoportal.dane.gov.co/laboratorio/serviciosjson/ipm/ipm_manzana.php?cod_dpto=' + mpio.substring(0, 2) + '&cod_mpio=' + mpio;
        } else {
            valorCampo = 'VARIABLE'
            if ($var.Map.varVariable.substring(0, 3) == 225) {
                urlData = 'http://geoportal.dane.gov.co/laboratorio/serviciosjson/ipm/ipm_condicion.php?cod_dpto=' + mpio.substring(0, 2) + '&cod_mpio=' + mpio + '&' + dataFiltro + '=1';
            }
            if ($var.Map.varVariable.substring(0, 3) == 224) {
                urlData = 'http://geoportal.dane.gov.co/laboratorio/serviciosjson/ipm/ipm_hogar_riesgo.php?cod_dpto=' + mpio.substring(0, 2) + '&cod_mpio=' + mpio + '&' + dataFiltro + '=1';
            }
            if ($var.Map.varVariable.substring(0, 3) == 223) {
                urlData = 'http://geoportal.dane.gov.co/laboratorio/serviciosjson/ipm/ipm_hacinamiento.php?cod_dpto=' + mpio.substring(0, 2) + '&cod_mpio=' + mpio + '&' + dataFiltro + '=1';
            }
        }
        $.ajax({
            url: urlData,
            dataType: "JSON",
            type: "GET",
            success: function (dataDos) {
                console.log(dataDos)
                if (dataDos.estado) {
                    $var.Map.dataManzana.options.vectorTileLayerStyles['riesgo-7nrtbm'] = function (properties, zoom) {
                        var style = {
                            fill: true, fillColor: 'green', fillOpacity: 1,
                            color: 'rgb(0, 0, 0)', opacity: 1, weight: 0.5,
                        };
                        return style;
                    },
                        console.log("aqui")
                    // $var.Map.dataManzana.redraw();
                }
            }
        });
    }

}

