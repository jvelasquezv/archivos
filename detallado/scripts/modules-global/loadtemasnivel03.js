var $var = require("../global/variables");//importar variables globales
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
$var.Map.loadtemasnivel03 = function() {
	var urlVariable = $var.Map.urlTemas + '?codigo=' + $var.Map.codigoVisor + '&variables=' + 'yes';
	$.ajax({
		url: urlVariable,
		type: "GET",
		success: function (data) {
			var firstTematica = false
			if (data.length > 0) {
				for (var i = 0; i < data.length; i++) {
					$var.Map.listaVariables[data[i]["COD_CATEGORIA"]] = [data[i]["CAMPO_TABLA"].toLowerCase(), data[i]["TIPO_VARIABLE"], data[i]["CATEGORIA"]];
					if (data[i]["NIVELES_FILTRO"] == "0") {
						$var.Map.listaFilterVariables.push([data[i]["COD_CATEGORIA"], data[i]["CAMPO_TABLA"]]);
						// $var.Map.listaVariables.push([data[i]["COD_CATEGORIA"], data[i]["CAMPO_TABLA"]]);
					}
					else if (data[i]["NIVELES_FILTRO"] == "1") {
						$var.Map.listaFilterVariables.push([data[i]["COD_CATEGORIA"], data[i]["CAMPO_DOMINIO"]]);
						// $var.Map.listaVariables.push([data[i]["COD_CATEGORIA"], data[i]["VALOR_DOMINIO"]]);
					}
					else if (data[i]["NIVELES_FILTRO"] == "2") {
						$var.Map.listaFilterVariables.push([data[i]["COD_CATEGORIA"], data[i]["CAMPO_DOMINIO2"]]);
						// $var.Map.listaVariables.push([data[i]["COD_CATEGORIA"], data[i]["VALOR_DOMINIO2"]]);
					}
					else if (data[i]["NIVELES_FILTRO"] == "3") {
						$var.Map.listaFilterVariables.push([data[i]["COD_CATEGORIA"], data[i]["CAMPO_DOMINIO3"]]);
						// $var.Map.listaVariables.push([data[i]["COD_CATEGORIA"], data[i]["VALOR_DOMINIO3"]]);
					}
					$var.Map.listaNivelesFiltro.push([data[i]["COD_CATEGORIA"], data[i]["NIVELES_FILTRO"]]);
					$var.Map.listaNombresVariables.push([data[i]["COD_CATEGORIA"], data[i]["CATEGORIA"]]);
					$var.Map.listaVisualizacion.push([data[i]["COD_CATEGORIA"], data[i]["GRAFICO_ESTADISTICO"]]);
					$var.Map.listaColores.push([data[i]["COD_CATEGORIA"], data[i]["COLOR"].replace("(", "").replace(")", "")]);
					$var.Map.listaUnidades.push([data[i]["COD_CATEGORIA"], data[i]["UNIDAD"]]);
					$var.Map.listaAdicional.push([data[i]["COD_CATEGORIA"], data[i]["CATEGORIA"]]);
					$var.Map.listaDescripcion.push([data[i]["COD_CATEGORIA"], data[i]["CATEGORIA"]]);
					$var.Map.listaTotal.push([data[i]["COD_CATEGORIA"], data[i]["TOTAL"]]);
					$var.Map.listaClaseTiene.push([data[i]["COD_CATEGORIA"], data[i]["FILTRO_CLASE"]]);
					$var.Map.listaSexo.push([data[i]["COD_CATEGORIA"], data[i]["FILTRO_SEXO"]]);
					$var.Map.listaAnioTiene.push([data[i]["COD_CATEGORIA"], data[i]["FILTRO_TIEMPO"]]);
					$var.Map.listaCompuestoTiene.push([data[i]["COD_CATEGORIA"], data[i]["VISUAL_AGRUPADO"]]);
					$var.Map.listaTipo.push([data[i]["COD_CATEGORIA"], data[i]["TIPO_VARIABLE"]]);
					if (data[i]["CATEGORIA"] != "Total" && data[i]["CATEGORIA"] != "No Informa" && data[i]["CATEGORIA"] != "No responde" && data[i]["CATEGORIA"] != "Sin información") { //
						if (data[i]["COD_SUBGRUPO"] == $(".level__2.--active").attr("id")) {
							if (!firstTematica) {
								$('<li class="filter__tematicas__selectList__item__content__item level__3' + data[i]["COD_CATEGORIA"] + '" id="' + data[i]["COD_CATEGORIA"] + '"><div class="filter__tematicas__selectList__item__btn"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["CATEGORIA"] + '</p></div></li>').appendTo($(".--variables-tematicas." + data[i]["COD_SUBGRUPO"]));
								firstTematica = true;
							}
							else {
								$('<li class="filter__tematicas__selectList__item__content__item level__3 ' + data[i]["COD_CATEGORIA"] + '" id="' + data[i]["COD_CATEGORIA"] + '"><div class="filter__tematicas__selectList__item__btn"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["CATEGORIA"] + '</p></div></li>').appendTo($(".--variables-tematicas." + data[i]["COD_SUBGRUPO"]));
							}
						}
						else {
							$('<li class="filter__tematicas__selectList__item__content__item level__3 ' + data[i]["COD_CATEGORIA"] + '" id="' + data[i]["COD_CATEGORIA"] + '"><div class="filter__tematicas__selectList__item__btn"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["CATEGORIA"] + '</p></div></li>').appendTo($(".--variables-tematicas." + data[i]["COD_SUBGRUPO"]));
						}
					}
				}
				$("#22601").addClass("--active");
			}
			$var.Map.accordiontemas(); //llama a la animacion de los acordeones de los temas, se coloca aqui porque despues de pintar por web service las funcionalidades como estas se colocan al final de pintarlos, para que los renconozca.
		}
	});

}
	