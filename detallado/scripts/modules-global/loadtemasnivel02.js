var $var = require("../global/variables");//importar variables globales
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
$var.Map.loadtemasnivel02 = function(){
	var servicioTemas = $var.Map.urlTemas + '?codigo=' + $var.Map.codigoVisor + '&sub_temas=' + 'yes';
	$.ajax({
		url: servicioTemas,
		type: "GET",
		success: function (data) {
			if (data.length > 0) {
				var firstTematica = false
				for (var i = 0; i < data.length; i++) {
					//Este if es para pintar el primer valor por defecto en los filtros tematicos, si no los muestra como no activos
					if ($(".filter__tematicas__selectList__item__btn.--nameTema.--active").parent().is('#'+data[i]["COD_GRUPO"]+'')) {
						if (!firstTematica) {							
							$('<div class="filter__tematicas__selectList__item level__2 --active " id="' + data[i]["COD_SUBGRUPO"] + '"><div class="filter__tematicas__selectList__item__btn --nameSubtema --active"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["SUBGRUPO"] + '</p></div><div class="filter__tematicas__selectList__item__content --active"><ul class="filter__tematicas__selectList__itemselectList --variables-tematicas ' + data[i]["COD_SUBGRUPO"] + '"></ul></div></div>').appendTo($("#" + data[i]["COD_GRUPO"] + " .filter__tematicas__selectList__item__subtemasList"));
							firstTematica = true;
							$(".toolbar__container__description__text").text(data[i]["DESCRIPCION_SUBGRUPO"]);
						}
						else {
							console.log('por aquiii')
							$('<div class="filter__tematicas__selectList__item level__2" id="' + data[i]["COD_SUBGRUPO"] + '"><div class="filter__tematicas__selectList__item__btn --nameSubtema"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["SUBGRUPO"] + '</p></div><div class="filter__tematicas__selectList__item__content "><ul class="filter__tematicas__selectList__itemselectList --variables-tematicas ' + data[i]["COD_SUBGRUPO"] + '"></ul></div></div>').appendTo($("#" + data[i]["COD_GRUPO"] + " .filter__tematicas__selectList__item__subtemasList"));
						}
					}
					else {
						$('<div class="filter__tematicas__selectList__item level__2" id="' + data[i]["COD_SUBGRUPO"] + '"><div class="filter__tematicas__selectList__item__btn --nameSubtema"><div class="filter__tematicas__selectList__item__btn__radio"><span class="DANE__Geovisor__icon__radioButton"></span></div><p class="filter__tematicas__selectList__item__btn__name">' + data[i]["SUBGRUPO"] + '</p></div><div class="filter__tematicas__selectList__item__content "><ul class="filter__tematicas__selectList__itemselectList --variables-tematicas ' + data[i]["COD_SUBGRUPO"] + '"></ul></div></div>').appendTo($("#" + data[i]["COD_GRUPO"] + " .filter__tematicas__selectList__item__subtemasList"));
					}
				}
			}
		}
	});
}