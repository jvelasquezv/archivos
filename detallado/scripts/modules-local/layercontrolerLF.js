//Este js para la funcionalidad de los mapas bases del visor
var $var = require("../global/variables");//importar variables globales
// Capa evento click de capas bases 
$(".layers__list__item__check").on("click", function () { 
    if ($(this).parent().hasClass("--ResguardosIndigenas")) {        
        capasReferencia($(this).parent().attr("id"),'resguardos','loadResguardos');
    }
    else if ($(this).parent().hasClass("--Parques")) {
        capasReferencia($(this).parent().attr("id"),"pnn",'loadParques');
    }
    else if ($(this).parent().hasClass("--ConsejosComunitariosNegros")) {
        capasReferencia($(this).parent().attr("id"),'consejos','loadConsejos');
    }
    else if ($(this).parent().hasClass("--ZonasDeReservaCampesina")) {
        capasReferencia($(this).parent().attr("id"),'campesinos','loadCampesinos');
    }
    else if ($(this).parent().hasClass("--Veredas")) {
        capasReferencia($(this).parent().attr("id"),'veredas','loadVeredas');   
    }
})
//Capas
function capasReferencia(clase,layer,funcion){
    var checkSelected = ".layers__list__item."+clase+"";
    if ($(checkSelected).hasClass(""+clase+"")) {
        if ($(checkSelected).hasClass("--visible")) {
            $(checkSelected).removeClass("--visible");
            var capa = '$var.Map.layers.'+layer
            // console.log('capa',capa)
            $var.Map.map.removeLayer(eval(capa));
        }
        else {
            $(checkSelected).addClass("--visible");
            // $(".loader").addClass('--active');
            // eval("$var.Map."+funcion+"()");
            var capa = '$var.Map.layers.'+layer
            $var.Map.map.addLayer(eval(capa));
        }
    }
}