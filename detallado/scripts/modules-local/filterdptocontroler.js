var $var = require("../global/variables");//importar variables globales
//Filtros Accion - Visualizar la lista divipola/departamentos
$('#filter__departamento').click(function (e) {
	console.log("oe")
	e.stopPropagation();//Click - para evitar que otras funcionalidades afecten al click hecho sobre el item
  	$('#filter__departamento .filter__selectBox__list').toggleClass("--active"); //Click - para quitar o ver la lista de departamentos cada vez que hagan click en el select
	$('#filter__municipio .filter__selectBox__list').removeClass("--active"); //Click - se quita la posibilidad de que se vea la lista de municipios para que no se monten entre ellas
});

//Filtros Accion - son las acciones que se asignan por teclado y enter de la lista de departamentos
var valuedpto = 0; // Valor - por defecto de la tecla
$("#filter__departamento").on("keyup", function(event) {
	var key = event.which;	
	$('#filter__departamento .filter__selectBox__list').addClass("--active");
	if(key === 13){// Filtro departamento - key 13 - valor "enter"
		$('#filter__departamento .filter__selectBox__list').removeClass("--active");//Lista - se quita la clase active que permite la visualizacion de la lista cuando se aplicuando haga "enter"
		$('#filter__municipio').removeClass("--invisible");	//Click - para visualizar el filtro de municipios se quita la clase --invisible	
		$('#filter__depto').val($(".filter__selectBox__list__item.--highlight").text());// Texto - para que en el input salga el valor del departamento seleccionado
		$var.Map.deptovalue = $(".filter__selectBox__list__item.--highlight").find(".filter__selectBox__list__item__text__code").text();// Value - Para asignar le nuevo valor del departamento seleccionado
		if($var.Map.deptovalue == "00"){//Si el v
			$("#filter__depto").text("00 - Todos los departamentos") //Filtro - asignar valor al input de todos los departamentos
			$('#filter__municipio').addClass("--invisible");	
			$("#filter__municipio .filter__selectBox__list").html("");
			$var.Map.listaDivipolaMpios = []
		}else{
			$var.Map.loadMunicipios(); //Ir a la funcion para cargar los municipios - loadmpios.js
		}
	}
	else if(key == 40){// Filtro departamento - key 40 - valor "down arrow"
		valuedpto++;//Valor - Suma el valor cuando haga click en la flecha por techado de abajo
		if(valuedpto > $(".filter__selectBox__list__item").length - 1){valuedpto= 0;}//Validación - No puede existir valor mayor a el tamaño de la lista se asigna el valor de cero para que empece de nuevo
		$(".filter__selectBox__list__item").removeClass("--highlight");//Item - se quita la clase de resalte a la lista 
		$(".filter__selectBox__list__item").eq(valuedpto).addClass("--highlight"); //Item - Asignar al item seleccionado la clase que lo resalta visualmente
	}
   	else if(key == 38){// Filtro departamento - key 38 - valor "up arrow"
		valuedpto--;//Valor - Resta el valor cuando haga click en la flecha por techado de abajo
		if(valuedpto < 0){valuedpto= $(".filter__selectBox__list__item").length - 1;}
		$(".filter__selectBox__list__item").removeClass("--highlight");//Item - se quita la clase de resalte a la lista 
		$(".filter__selectBox__list__item").eq(valuedpto).addClass("--highlight");//Item - Asignar al item seleccionado la clase que lo resalta visualmente
   	}
});