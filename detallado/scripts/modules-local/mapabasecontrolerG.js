//Este js para la funcionalidad de los mapas bases del visor
var $var = require("../global/variables");//importar variables globales
//cambio de mapa cuando se hace click en los botones  
$(".basemap__list__item").on("click", function(){ 
    $(".basemap__list__item").removeClass("--active");
    $(this).addClass("--active");
    if($(this).attr("id") == "basemap__gris"){
        $var.Map.loadGris();
    }
    else if($(this).attr("id") == "basemap__satelital"){
        $var.Map.loadSatelital();
    }
    else if($(this).attr("id") == "basemap__noche"){
        $var.Map.loadNoche();
    }
    else if($(this).attr("id") == "basemap__osm"){
        $var.Map.loadOSM();
    }
    else if($(this).attr("id") == "basemap__terreno"){
        $var.Map.loadTerreno();
    }
    else if($(this).attr("id") == "basemap__hibrido"){
        $var.Map.loadHibrido();
    }
})