var $var = require("../global/variables");//Variables - importar variables globales
//Filtros Accion - Visualizar la lista de municipios del departamento seleccionado
$('#filter__municipio').click(function (e) {
	e.stopPropagation();//Click - para evitar que otras funcionalidades afecten al click hecho sobre el item
  	$('#filter__municipio .filter__selectBox__list').toggleClass("--active"); //Click - para quitar o ver la lista de departamentos cada vez que hagan click en el select
	$('#filter__departamento .filter__selectBox__list').removeClass("--active"); //Click - se quita la posibilidad de que se vea la lista de departamentos para que no se monten entre ellas
});

//Filtros Accion - son las acciones que se asignan por teclado y enter de la lista de municipios
var valuempio = 0;
$("#filter__municipio").on("keyup", function(event) {
	var key = event.which;	
	$('#filter__municipio .filter__selectBox__list').addClass("--active");
	if(key === 13){// Filtro departamento - key 13 - valor "enter"
		$('#filter__municipio .filter__selectBox__list').removeClass("--active");
		$('#filter__municipio').removeClass("--invisible");
		$('#filter__mpio').val($(".filter__selectBox__list__item.--highlight").text());
		$var.Map.mpiovalue = $(".filter__selectBox__list__item.--highlight").find(".filter__selectBox__list__item__text__code").text();
	}
	else if(key == 40){// Filtro departamento - key 40 - valor "down arrow"
		valuempio++;
		if(valuempio > $(".filter__selectBox__list__item").length - 1){valuempio= 0;        }
		$(".filter__selectBox__list__item").removeClass("--highlight");
		$(".filter__selectBox__list__item").eq(valuempio).addClass("--highlight"); 
	}
   	else if(key == 38){// Filtro departamento - key 38 - valor "up arrow"
	   valuempio--;
		if(valuempio < 0){valuempio= $(".filter__selectBox__list__item").length - 1;}
		$(".filter__selectBox__list__item").removeClass("--highlight");
		$(".filter__selectBox__list__item").eq(valuempio).addClass("--highlight");
   	}
});