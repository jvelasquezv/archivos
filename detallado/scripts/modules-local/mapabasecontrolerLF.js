//Este js para la funcionalidad de los mapas bases del visor
var $var = require("../global/variables");//importar variables globales
//cambio de mapa cuando se hace click en los botones  
$(".basemap__list__item").on("click", function(){ 
    $(".basemap__list__item").removeClass("--active");
    $(this).addClass("--active");
    // var capa = '$var.Map.'+ $(this).attr("id")
    $var.Map.map.removeLayer($var.Map.loadGris);
    $var.Map.map.removeLayer($var.Map.loadSatelital);
    $var.Map.map.removeLayer($var.Map.loadNoche);
    $var.Map.map.removeLayer($var.Map.loadOSM);
    $var.Map.map.removeLayer($var.Map.loadTerreno);
    $var.Map.map.removeLayer($var.Map.loadHibrido);

    if($(this).attr("id") == "basemap__gris"){  
        // baseLayers["Tile Layer 1"]($var.Map.loadGris).addTo($var.Map.map);      
        $var.Map.map.addLayer($var.Map.loadGris);
    }
    else if($(this).attr("id") == "basemap__satelital"){
        // L.control.layers($var.Map.loadSatelital).addTo($var.Map.map);
        $var.Map.map.addLayer($var.Map.loadSatelital);
    }
    else if($(this).attr("id") == "basemap__noche"){
        // L.control.layers($var.Map.loadNoche.addTo($var.Map.map));
        $var.Map.map.addLayer($var.Map.loadNoche);
    }
    else if($(this).attr("id") == "basemap__osm"){
        // L.control.layers($var.Map.loadOSM).addTo($var.Map.map);
        $var.Map.map.addLayer($var.Map.loadOSM);
    }
    else if($(this).attr("id") == "basemap__terreno"){
        // L.control.layers($var.Map.loadTerreno).addTo($var.Map.map);
        $var.Map.map.addLayer($var.Map.loadTerreno);
    }
    else if($(this).attr("id") == "basemap__hibrido"){
        // L.control.layers($var.Map.loadHibrido).addTo($var.Map.map);
        $var.Map.map.addLayer($var.Map.loadHibrido);
    }
    // $var.Map.map.eachLayer(function(layer){
    //     console.log(layer)
    // });  
})