//Este js para la funcionalidad de los mapas bases del visor
var $var = require("../global/variables");//importar variables globales
// Capa evento click de capas bases 
$(".layers__list__item__check").on("click", function () { 
    if ($(this).parent().hasClass("--ResguardosIndigenas")) {
        capasReferencia($(this).parent().attr("id"),'resguardos',"Resguardos",'loadResguardos');
    }
    else if ($(this).parent().hasClass("--Parques")) {
        capasReferencia($(this).parent().attr("id"),'pnn',"Parques",'loadParques');
    }
    else if ($(this).parent().hasClass("--ConsejosComunitariosNegros")) {
        capasReferencia($(this).parent().attr("id"),'consejos',"Consejos",'loadConsejos');
    }
    else if ($(this).parent().hasClass("--ZonasDeReservaCampesina")) {
        capasReferencia($(this).parent().attr("id"),'campesinos',"Campesina",'loadCampesinos');
    }
    else if ($(this).parent().hasClass("--Veredas")) {
        capasReferencia($(this).parent().attr("id"),'veredas',"Veredas",'loadVeredas');   
    }
})
//Capas
function capasReferencia(clase,layer,name,funcion){
    var checkSelected = ".layers__list__item."+clase+"";
    if ($(checkSelected).hasClass(""+clase+"")) {
        if ($(checkSelected).hasClass("--visible")) {
            $(checkSelected).removeClass("--visible");
            var pos = null;
            $var.Map.map.overlayMapTypes.forEach(function (d, i) {
                if (d.name == ""+name+"") {
                    pos = i;
                }
            })
            if (pos != null) {
                $var.Map.map.overlayMapTypes.removeAt(pos)
            }
            $var.Map.layers3D.layer.remove();    
        }
        else {
            $(checkSelected).addClass("--visible");
            $(".loader").addClass('--active');
            // console.log("$var.Map."+funcion+"()")
            eval("$var.Map."+funcion+"()");
        }
    }
}