var $var = require("../global/variables");//importar variables globales
$var.Map.accessibility = function(){
    var fontsize = 1;
    $('.accessibility__list__item__plus').on("click", function(){
        fontsize = fontsize + 0.05;
        if(fontsize >= 1.25){
            fontsize= fontsize;
        }else{
            $("body").css({"font-size":fontsize + "em"});
        }        
    });
    $('.accessibility__list__item__minus').on("click", function(){
        fontsize = fontsize - 0.05;
        if(fontsize <= 1){
            fontsize=1;
            $("body").removeAttr("style");
        }else{
            $("body").css({"font-size":fontsize + "em"});
        }
    });
    $('.--color').on("click", function(){
        $("body").toggleClass('--grayscale');      
    });
}