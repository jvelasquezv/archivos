var $var = require("../global/variables");//importar variables globales
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
$var.Map.accordiontemas = function() {
	//funcionalidad para acordeones de primer nivel de los filtros tematicos
	for (var t=document.getElementsByClassName("filter__tematicas__typebtn"), n=0;n<t.length;n++) {
			t[n].addEventListener("click",function(){
			this.classList.toggle("--active");
			var t=this.nextElementSibling;
			$(t).toggleClass("--open")
		});		
	}
	//funcionalidad para acordeones de segundo nivel de los filtros tematicos
	for (var t=document.getElementsByClassName("--nameTema"), n=0;n<t.length;n++) {
		t[n].addEventListener("click",function(){
			this.classList.toggle("--active");
			var t=this.nextElementSibling;
			$(t).toggleClass("--active")
		});		
	}
	//funcionalidad para acordeones de tercer nivel de los filtros tematicos de las variables
	for (var t=document.getElementsByClassName("--nameSubtema"), n=0;n<t.length;n++) {
		t[n].addEventListener("click",function(){
			this.classList.toggle("--active");
			var t=this.nextElementSibling;
			$(t).toggleClass("--active")
		});		
	}
	//Click en la variable para activarla
	$(".filter__tematicas__selectList__item__content__item.level__3").click(function(){
		$(".filter__tematicas__selectList__item__content__item.level__3").removeClass("--active");
		$(this).addClass("--active");
		// console.log($var.Map.listaVariables);
		// console.log($(this).attr("id"));
		$var.Map.varVariable = $(this).attr("id");
	});
}