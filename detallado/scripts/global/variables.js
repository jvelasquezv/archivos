exports.Map ={
	codigoVisor: 15, //Codigo del visor -segun la asignacion de base de datos Estr_Geovisor
	latInicial: 4.181390112341817, //latitud inicial - Ubicación DANE
	lngInicial: -74.34930964783457, //longitud inicial - Ubicación DANE
	zoomLevel: 6, //Zoom Inicial
	listaDivipolaDeptos: [], //Arreglo que guarda la lista divipola - loaddivipola	.js
	listaDivipolaMpios:[],//Arreglo que guarda la lista de los departamentos del departamento elegido - loaddivipola.js
	loadDivipola: null, //definicion global de la funcion loaddivipola - loaddivipola.js
	filterGeografico: null, //definicion global de la funcion filterGeografico - filter.js
	deptovalue:'00',//valor de departamento por defecto, el 00 es para cuando el valor es igual a todos los departamentos
	mpiovalue:'00000',//valor de municipio por defecto, el 00000 es para cuando el valor es igual a todos los municipios
	loadMunicipios:null,//definicion global de la funcion loaddmunicipios - loaddivipola.js
	urlDivipola:'https://geoportal.dane.gov.co/laboratorio/serviciosjson/cnpv/divipola.php', //enlace/servicio  que trae el servicio de los filtros geograficos
	urlTemas: 'https://geoportal.dane.gov.co/laboratorio/serviciosjson/visores/temas.php', //enlace/servicio  que trae el servicio de las tematicas por visor
	urlVariables : 'https://geoportal.dane.gov.co/laboratorio/serviciosjson/visores/variables.php',//enlace/servicio  que trae los datos de la variable seleccionada
	loadtemasnivel01: null, //definicion global de la funcion loadTematicas - loadtemasnivel01.js
	unique: null, //definicion de la funcion unique para sacar  los valores unicos en un array - unique.js
	getTipoTematica: null, //definicion de la funcion para definir el tipo de tematica que es - getTipoTematica.js
	loadtemasnivel02: null, //definicion de las tematicas que van por tema  segundo nivel - loadtemasnivel02.js
	loadtemasnivel03: null, //definicion de las las variables de las tematicas  tercer nivel - loadtemasnivel03.js
	listaFilterVariables:  [], //Arreglo para guardar la lista de variables en los filtros - loadtemasnivel03.js
	listaVariables: [], // Arreglo para guardar las variables - loadtemasnivel03.js
	listaNivelesFiltro: [], // Arreglo para guardar los niveles del filtro - loadtemasnivel03.js
	listaNombresVariables: [], // Arreglo para guardar los nombres de las variables - loadtemasnivel03.js
	listaVisualizacion: [], // Arreglo para guardar la lista de visualizacion - loadtemasnivel03.js
	listaColores: [], // Arreglo para guardar la lista de colores - loadtemasnivel03.js
	listaUnidades: [], // Arreglo para guardar las unidades de las valiables - loadtemasnivel03.js
	listaAdicional:[], // Arreglo para guardar la lista adicional de las variables - loadtemasnivel03.js
	listaDescripcion:[], // Arreglo para guardar la descripcioon de las variables - loadtemasnivel03.js
	listaTotal:[], // Arreglo para guardar las variables - loadtemasnivel03.js
	listaClaseTiene:[], // Arreglo para guardar el total de las variables - loadtemasnivel03.js
	listaSexo:[], // Arreglo para guardar el sexo al que pertenencen las variables - loadtemasnivel03.js
	listaAnioTiene:[], // Arreglo para guardar el anio que tienen las variables - loadtemasnivel03.js
	listaCompuestoTiene:[], // Arreglo para guardar el compuesto de las variables - loadtemasnivel03.js
	listaTipo:[], // Arreglo para guardar el tipo al que pertenece la variable las variables - loadtemasnivel03.js
	accordiontemas: null, //funcion para la funcionalidad de los  acordeones despues de imprimir temas - accordiontemas.js
	mapasbase: null, //funcion para los mapas base del mapa principal - mapasbase.js
	layers:null, //funcion para hacer las capas - layer.js
	loadestadisticas:null,//funcion para llenar el arreglo  con las estadisicas de las variables seleccionadas loadestadisticas.
	accessibility:null,//funcion que aplica los acjustes de accesisilidad como tamaño de la fuentes y cambio de colores accessibility.js
	map:null, //variable de mapa principal del visor
	percentageMapOpacity:1, //Porgentaje de opacidad de la capa layer
	layers: {
	  	mgn2005: null,
	  	mgn2012: null,
	  	mgn2017: null,
	  	data: null,
	  	kmlLayer: null,
	  	pnn: null,
	  	consejos: null,
	  	campesinos: null,
	  	veredas: null,
	  	base_dptos: null,
	  	resguardos:null
	},
	mapOptions:null,
	basemapdefault: [],
	basemapgris: [{"elementType": "geometry","stylers": [{"color": "#f5f5f5"}]},{"elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#616161"}]},{"elementType": "labels.text.stroke","stylers": [    {    "color": "#f5f5f5"    }]},{"featureType": "administrative.land_parcel","elementType": "labels.text.fill","stylers": [{"color": "#bdbdbd"}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#ffffff"}]},{"featureType": "road.arterial","elementType": "labels.text.fill","stylers": [{"color": "#757575"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#dadada"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [    {    "color": "#616161"    }]},{"featureType": "road.local","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]},{"featureType": "transit.line","elementType": "geometry","stylers": [{"color": "#e5e5e5"}]},{"featureType": "transit.station","elementType": "geometry","stylers": [{"color": "#eeeeee"}]},{"featureType": "water","elementType": "geometry","stylers": [{"color": "#c9c9c9"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#9e9e9e"}]}],
	basemapnoche: [{"elementType": "geometry","stylers": [{"color": "#1d2c4d"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#8ec3b9"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#1a3646"}]},{"featureType": "administrative.country","elementType": "geometry.stroke","stylers": [{"color": "#4b6878"}]},{"featureType": "administrative.land_parcel","elementType": "labels.text.fill","stylers": [{"color": "#64779e"}]},{"featureType": "administrative.province","elementType": "geometry.stroke","stylers": [    {    "color": "#4b6878"    }]},{"featureType": "landscape.man_made","elementType": "geometry.stroke","stylers": [{"color": "#334e87"}]},{"featureType": "landscape.natural","elementType": "geometry","stylers": [{"color": "#023e58"}]},{"featureType": "poi","elementType": "geometry","stylers": [{"color": "#283d6a"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#6f9ba5"}]},{"featureType": "poi","elementType": "labels.text.stroke","stylers": [{"color": "#1d2c4d"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#023e58"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#3C7680"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#304a7d"}]},{"featureType": "road","elementType": "labels.text.fill","stylers": [{"color": "#98a5be"}]},{"featureType": "road","elementType": "labels.text.stroke","stylers": [{"color": "#1d2c4d"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#2c6675"}]},{"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#255763"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#b0d5ce"}]},{"featureType": "road.highway","elementType": "labels.text.stroke","stylers": [{"color": "#023e58"}]},{"featureType": "transit","elementType": "labels.text.fill","stylers": [{"color": "#98a5be"}]},{"featureType": "transit","elementType": "labels.text.stroke","stylers": [{"color": "#1d2c4d"}]},{"featureType": "transit.line","elementType": "geometry.fill","stylers": [{"color": "#283d6a"}]},{"featureType": "transit.station","elementType": "geometry","stylers": [{"color": "#3a4762"}]},{"featureType": "water","elementType": "geometry","stylers": [{"color": "#0e1626"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#4e6d70"}]}],
	loadGris:null,
	loadSatelital:null,
	loadNoche:null,
	loadOSM:null,
	loadTerreno:null,
	loadHibrido:null,
	loadResguardos:null,
	loadParques:null,
	loadConsejos:null,
	loadCampesinos:null,
	loadVulnerabilidad: null,
	loadMgn2018: null,
	varVariable: "22601001",
	dataManzana: null
};
