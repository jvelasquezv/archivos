//Estilos - zona para importar estilos
import './../../styles/combos/styles.scss'
var $var = require("../global/variables");//importar variables globales
import $ from 'jquery';
window.jQuery = $;
window.$ = $;


/***-----global----- ***/
import '../global/variables.js'
/***-----BASE----- ***/
import '../base/google.js'

/***-----modules-global----- ***/
import '../modules-global/toolbar.js'
import '../modules-global/loaddivipola.js'
import '../modules-global/loadmpios.js'
import '../modules-global/loadtemasnivel01.js'
import '../modules-global/loadtemasnivel02.js'
import '../modules-global/loadtemasnivel03.js'
import '../modules-global/map.js'
import '../modules-global/loadestadisticas.js'

/***-----modules-global----- ***/
import '../vendor-global/accordion.js'
import '../vendor-global/accordiontemas.js'

/***-----modules-local----- ***/
import '../modules-local/accessibility.js'
import '../modules-local/mapabasecontroler.js'
import '../modules-local/layercontroler.js'
import '../modules-local/filterdptocontroler.js'
import '../modules-local/filtermpiocontroler.js'

/***-----vendor-local----- ***/
import '../vendor-local/unique.js'
import '../vendor-local/getTipoTematica.js'
import '../vendor-local/layer.js'

import "../vendor-local/mapasbaseG.js"


$(document).ready(function () {
  $var.Map.loadDivipola(); //cargar filtros geograficos de la divipola/deptos
  $var.Map.loadMunicipios();//cargar filtros geograficos de los municipios
  $var.Map.loadtemasnivel01(); //cargar filtros tematicos nivel 1 y dividir categorias
  $var.Map.loadtemasnivel02();//cargar filtros tematicos nivel 2
  $var.Map.loadtemasnivel03();//cargar filtros tematicos nivel 3
  $var.Map.mapasbase();
  $var.Map.layers();
  $var.Map.accessibility();
});

