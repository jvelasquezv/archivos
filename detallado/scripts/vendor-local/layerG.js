//Este js para la funcionalidad de capas del visor
var $var = require("../global/variables");//importar variables globales
//Mapa base funcion - determina si hace click y coloca el mapa base por defecto este se lo asigna al ID del mapa base principal que encontraran en map.pug - #map__content__principalMap
$var.Map.layers = function(){
    basemapPrint();
}
function basemapPrint() {
    $.ajax({
        url:"https://geoportal.dane.gov.co/laboratorio/serviciosjson/visores/capasreferencia.php?nivel_capa=1",
        type:"GET",
        success: function(data){
            $.each(data.resultado, function (key, value) {
                var ul = $('.layers__list');
                var liTemp = ul.find('li:first').clone(true);
                liTemp.addClass(value["CLASE"]).attr('id', value["CLASE"]).removeClass("--invisible");
                liTemp.find(".layers__list__item__source__text").text(value["FUENTE_TEXTO"]);
                liTemp.find(".layers__list__item__source__link").text(value["FUENTE_ENLACE"]).attr('href', value["FUENTE_ENLACE"]);
                liTemp.find(".layers__list__item__name").text(value["NOMBRE"]);               
                liTemp.appendTo(ul);
            });
        }       
    })
}
//Funcion para visualizar la capa de resguardos indigenas
$var.Map.loadResguardos = function(){
    var map = $var.Map.map
    $var.Map.layers.resguardos = new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            var proj = map.getProjection();
            var zfactor = Math.pow(2, zoom);
            // get Long Lat coordinates
            var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
            var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
            //corrections for the slight shift of the SLP (mapserver)
            var deltaX = 0.0013;
            var deltaY = 0.00058;
            //create the Bounding box string
            var bbox = (top.lng() + deltaX) + "," +
                (bot.lat() + deltaY) + "," +
                (bot.lng() + deltaX) + "," +
                (top.lat() + deltaY);
            //base WMS URL
            var url = "https://geoserverportal.dane.gov.co/geoserver2/dig/wms?";
            url += "VERSION=1.1.1"; //WMS operation
            url += "&REQUEST=GetMap"; //WMS operation
            url += "&LAYERS=" + "dig:RESGUARDOS_INDIGENAS"; //WMS layers
            url += "&FORMAT=image/png";
            url += "&TRANSPARENT=true";     //set WGS84 
            url += "&SRS=EPSG:4326";     //set WGS84 
            url += "&BBOX=" + bbox;      // set bounding box
            url += "&WIDTH=256";         //tile size in google
            url += "&HEIGHT=256";
            return url;                 // return URL for the tile

        },
        tileSize: new google.maps.Size(256, 256),
        opacity: 1,
        name: "Resguardos",
        isPng: true
    });
    map.overlayMapTypes.push($var.Map.layers.resguardos);
    $(".loader").removeClass('--active');
}
//funcion para cargar los parques
$var.Map.loadParques = function(){
    var map = $var.Map.map
    $var.Map.layers.pnn = new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            var proj = map.getProjection();
            var zfactor = Math.pow(2, zoom);
            // get Long Lat coordinates
            var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
            var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
            //corrections for the slight shift of the SLP (mapserver)
            var deltaX = 0.0013;
            var deltaY = 0.00058;
            //create the Bounding box string
            var bbox = (top.lng() + deltaX) + "," +
                (bot.lat() + deltaY) + "," +
                (bot.lng() + deltaX) + "," +
                (top.lat() + deltaY);
            //base WMS URL
            var url = "https://geoserverportal.dane.gov.co/geoserver2/dig/wms?";
            url += "VERSION=1.1.1"; //WMS operation
            url += "&REQUEST=GetMap"; //WMS operation
            url += "&LAYERS=" + "dig:PARQUES_NATURALES"; //WMS layers
            url += "&FORMAT=image/png";
            url += "&TRANSPARENT=true";     //set WGS84 
            url += "&SRS=EPSG:4326";     //set WGS84 
            url += "&BBOX=" + bbox;      // set bounding box
            url += "&WIDTH=256";         //tile size in google
            url += "&HEIGHT=256";
            return url;                 // return URL for the tile

        },
        tileSize: new google.maps.Size(256, 256),
        opacity: 1,
        name: "Parques",
        isPng: true
    });
    map.overlayMapTypes.push($var.Map.layers.pnn);
    $(".loader").removeClass('--active');
}
$var.Map.loadConsejos = function(){
    var map = $var.Map.map
    $var.Map.layers.consejos = new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            var proj = map.getProjection();
            var zfactor = Math.pow(2, zoom);
            // get Long Lat coordinates
            var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
            var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
            //corrections for the slight shift of the SLP (mapserver)
            var deltaX = 0.0013;
            var deltaY = 0.00058;
            //create the Bounding box string
            var bbox = (top.lng() + deltaX) + "," +
                (bot.lat() + deltaY) + "," +
                (bot.lng() + deltaX) + "," +
                (top.lat() + deltaY);
            //base WMS URL
            var url = "https://geoserverportal.dane.gov.co/geoserver2/dig/wms?";
            url += "VERSION=1.1.1"; //WMS operation
            url += "&REQUEST=GetMap"; //WMS operation
            url += "&LAYERS=" + "dig:CONSEJOS_COM_NEGROS"; //WMS layers
            url += "&FORMAT=image/png";
            url += "&TRANSPARENT=true";     //set WGS84 
            url += "&SRS=EPSG:4326";     //set WGS84 
            url += "&BBOX=" + bbox;      // set bounding box
            url += "&WIDTH=256";         //tile size in google
            url += "&HEIGHT=256";
            return url;                 // return URL for the tile
        },
        tileSize: new google.maps.Size(256, 256),
        opacity: 1,
        name: "Consejos",
        isPng: true
    });
    map.overlayMapTypes.push($var.Map.layers.consejos);
    $(".loader").removeClass('--active');
}
$var.Map.loadCampesinos = function(){
    var map = $var.Map.map
    $var.Map.layers.campesinos = new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
            var proj = map.getProjection();
            var zfactor = Math.pow(2, zoom);
            // get Long Lat coordinates
            var top = proj.fromPointToLatLng(new google.maps.Point(coord.x * 256 / zfactor, coord.y * 256 / zfactor));
            var bot = proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * 256 / zfactor, (coord.y + 1) * 256 / zfactor));
            //corrections for the slight shift of the SLP (mapserver)
            var deltaX = 0.0013;
            var deltaY = 0.00058;
            //create the Bounding box string
            var bbox = (top.lng() + deltaX) + "," +
                (bot.lat() + deltaY) + "," +
                (bot.lng() + deltaX) + "," +
                (top.lat() + deltaY);
            //base WMS URL
            var url = "https://geoserverportal.dane.gov.co/geoserver2/dig/wms?";
            url += "VERSION=1.1.1"; //WMS operation
            url += "&REQUEST=GetMap"; //WMS operation
            url += "&LAYERS=" + "dig:ZONAS_RESERVA_CAMPESINA"; //WMS layers
            url += "&FORMAT=image/png";
            url += "&TRANSPARENT=true";     //set WGS84 
            url += "&SRS=EPSG:4326";     //set WGS84 
            url += "&BBOX=" + bbox;      // set bounding box
            url += "&WIDTH=256";         //tile size in google
            url += "&HEIGHT=256";
            return url;                 // return URL for the tile

        },
        tileSize: new google.maps.Size(256, 256),
        opacity: 1,
        name: "Campesina",
        isPng: true
    });
    map.overlayMapTypes.push($var.Map.layers.campesinos);
    $(".loader").removeClass('--active');
}
