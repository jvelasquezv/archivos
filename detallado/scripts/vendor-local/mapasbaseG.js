//Este js para la funcionalidad de los mapas bases del visor
var $var = require("../global/variables");//importar variables globales
console.log('estoy en mapa base google')
//Mapa Base Gris
$var.Map.loadGris = function(){
    $var.Map.map.setMapTypeId('roadmap');
    $var.Map.map.setOptions({styles: $var.Map.basemapgris});
}
//Mapa Base Satelital
$var.Map.loadSatelital = function(){
    $var.Map.map.setMapTypeId('satellite');
    $var.Map.map.setOptions({styles: $var.Map.basemapdefault});
}
//Mapa Base Hibrido
$var.Map.loadHibrido = function(){
    $var.Map.map.setMapTypeId('hybrid');
    $var.Map.map.setOptions({styles: $var.Map.basemapdefault});
}
//Mapa Base Noche 
$var.Map.loadNoche = function(){
    $var.Map.map.setMapTypeId('roadmap');
    $var.Map.map.setOptions({styles: $var.Map.basemapnoche});
}
//Mapa Base OSM o default de google
$var.Map.loadOSM = function(){
    $var.Map.map.setMapTypeId('roadmap');
    $var.Map.map.setOptions({styles: $var.Map.basemapdefault});
}
//Mapa Base de terreno
$var.Map.loadTerreno = function(){
    $var.Map.map.setMapTypeId('terrain');
    $var.Map.map.setOptions({styles: $var.Map.basemapdefault});
}

//Mapa Configuración Básica
$var.Map.mapOptions = {
	center: {
		lat: $var.Map.latInicial,
		lng: $var.Map.lngInicial
	},
	zoom: $var.Map.zoomLevel,
	mapTypeControl: false,
	fullscreenControl: false,
	scaleControl: true,
	streetViewControl: true,
	zoomControl: false,
	draggable: true,
	styles: $var.Map.basemapgris
}; 

//Mapa Base - Configuración del servicio web de los mapas base
function layersPrint(){
    $.ajax({
        url:"https://geoportal.dane.gov.co/laboratorio/serviciosjson/visores/mapasbase.php?nivel_mapabase=1",
        type:"GET",
        success: function(data){
            $.each(data.resultado, function (key, value) {
                var ul = $('.basemap__list');
                var liTemp = ul.find('li:first').clone(true);
                liTemp.addClass(value["CLASE"]).attr('id', value["CLASE"]).removeClass("--invisible");
                var liDivTemp = liTemp.children(".basemap__list__item__btn");
                liTemp.find(".basemap__list__item__btn__imageBox__image").attr('title','Geoportal DANE - '+value["NOMBRE"]).attr('alt','Geoportal DANE - '+value["NOMBRE"]).attr('src', value["IMAGEN_ENLACE"]);
                liDivTemp.find(".basemap__list__item__btn__name").text(value["NOMBRE"]);               
                liTemp.appendTo(ul);
            });
            $('.basemap__gris').addClass('--active');
        }
    });
}
//Mapa base funcion - determina si hace click y coloca el mapa base por defecto este se lo asigna al ID del mapa base principal que encontraran en map.pug - #map__content__principalMap
$var.Map.mapasbase = function(){
	layersPrint();	 
	$var.Map.map = new google.maps.Map(document.getElementById('map__content__principalMap'),$var.Map.mapOptions); //Mapa Base por defecto configuración a partir del id del mapa
	$var.Map.map.setTilt(45);
}


