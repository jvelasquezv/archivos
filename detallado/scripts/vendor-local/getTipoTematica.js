var $var = require("../global/variables");

$var.Map.getTipoTematica = function(value){
  if(value == 0){
      return "Temas generales";
  }
  else if(value == 1){
      return "Enfoque de género";
  }
  else if(value == 2){
      return "Indicadores sociodemográficos";
  }
}