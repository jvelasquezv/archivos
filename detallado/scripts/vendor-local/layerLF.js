//Este js para la funcionalidad de capas del visor
var $var = require("../global/variables");//importar variables globales
//Mapa base funcion - determina si hace click y coloca el mapa base por defecto este se lo asigna al ID del mapa base principal que encontraran en map.pug - #map__content__principalMap
$var.Map.layers = function () {
    basemapPrint();
}
function basemapPrint() {
    $.ajax({
        url: "https://geoportal.dane.gov.co/laboratorio/serviciosjson/visores/capasreferencia.php?nivel_capa=1",
        type: "GET",
        success: function (data) {
            $.each(data.resultado, function (key, value) {
                var ul = $('.layers__list');
                var liTemp = ul.find('li:first').clone(true);
                liTemp.addClass(value["CLASE"]).attr('id', value["CLASE"]).removeClass("--invisible");
                liTemp.find(".layers__list__item__source__text").text(value["FUENTE_TEXTO"]);
                liTemp.find(".layers__list__item__source__link").text(value["FUENTE_ENLACE"]).attr('href', value["FUENTE_ENLACE"]);
                liTemp.find(".layers__list__item__name").text(value["NOMBRE"]);
                liTemp.appendTo(ul);
            });
        }
    })
}
//Funcion para visualizar la capa de resguardos indigenas
$var.Map.loadResguardos = function () {
    $var.Map.layers.resguardos = L.tileLayer.wms("https://geoserverportal.dane.gov.co/geoserver2/dig/wms", {
        layers: "dig:RESGUARDOS_INDIGENAS",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: "Resguardos"
    })
    return $var.Map.layers.resguardos;

}
//funcion para cargar los parques
$var.Map.loadParques = function () {
    $var.Map.layers.pnn = L.tileLayer.wms("https://geoserverportal.dane.gov.co/geoserver2/dig/wms", {
        layers: "dig:PARQUES_NATURALES",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: "Parques"
    })
    return $var.Map.layers.pnn;

}
$var.Map.loadConsejos = function () {
    $var.Map.layers.consejos = L.tileLayer.wms("https://geoserverportal.dane.gov.co/geoserver2/dig/wms", {
        layers: "dig:CONSEJOS_COM_NEGROS",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: "Consejos"
    })
    return $var.Map.layers.consejos;
}
$var.Map.loadCampesinos = function () {
    $var.Map.layers.campesinos = L.tileLayer.wms("https://geoserverportal.dane.gov.co/geoserver2/dig/wms", {
        layers: "dig:ZONAS_RESERVA_CAMPESINA",
        format: 'image/png',
        transparent: true,
        version: '1.3.0',
        attribution: "Campesinos"
    })
    return $var.Map.layers.campesinos;
}
$var.Map.loadVulnerabilidad = function () {
    var openmaptilesUrl = "https://api.mapbox.com/v4/ivan12345678.9t8jbmu0/{z}/{x}/{y}.vector.pbf?sku=101h6wrNEIHUF&access_token={key}";
    var openmaptilesVectorTileOptions = {
        rendererFactory: L.canvas.tile,
        attribution: '<a href="https://openmaptiles.org/">&copy; OpenMapTiles</a>, <a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap</a> contributors',
        // minNativeZoom: 10,
        vectorTileLayerStyles: {
            'riesgo-7nrtbm': function (properties, zoom) {
                // crearArray(properties);
                var cluster = properties.cluster
                return cargarStilos(cluster, zoom);
            }
        },
        interactive: true,
        key: 'pk.eyJ1IjoiaXZhbjEyMzQ1Njc4IiwiYSI6ImNqc2ZkOTNtMjA0emgzeXQ3N2ppMng4dXAifQ.2k-OLO6Do2AoH5GLOWt-xw',
        getFeatureId: function (e) {
            $var.Map.dataManzana.resetFeatureStyle(e.properties.id);
            return e.properties.id;
        }
    };
    $var.Map.dataManzana = L.vectorGrid.protobuf(openmaptilesUrl, openmaptilesVectorTileOptions)
        // http://geoportal.dane.gov.co/laboratorio/serviciosjson/ipm/ipm_condicion.php?cod_dpto=13&cod_mpio=13001&clase=1&p_sexo_1=1
        .on('click', function (e) {	// The .on method attaches an event handler
            // $var.Map.dataManzana.resetFeatureStyle(e.layer.properties.id);
            // $var.Map.dataManzana.setFeatureStyle(e.layer.properties.id, {
            //     weight: 0,
            //     fillColor: 'red',
            //     fillOpacity: 1,
            //     fill: true
            // })
            var pro = e.layer.properties;
            var HTML = '';
            for (var q in pro) {
                HTML += q + ":" + pro[q] + '<br />';
            }
            L.popup()
                .setContent(HTML)
                .setLatLng(e.latlng)
                .openOn($var.Map.map);
            L.DomEvent.stop(e);
        })
        .addTo($var.Map.map);
    return $var.Map.dataManzana;
}
function cargarStilos(cluster, zoom) {
    var color = '#9bc2c4'
    if (cluster === 0) { color = '#FFD0BE' };
    if (cluster === 1) { color = '#950000' };
    if (cluster === 2) { color = '#FF3E35' };
    if (cluster === 3) { color = '#FF955C' };
    if (cluster === 4) { color = '#FF6F35' };
    return {
        weight: 0,
        fillColor: color,
        fillOpacity: 1,
        fill: true
    }
}
$var.Map.loadMgn2018 = function () {
    var wmsLayer = L.tileLayer.wms('https://geoserverportal.dane.gov.co/geoserver2/postgis/wms?', {
        layers: 'postgis:mgn_2018',
        format: 'image/png',
        transparent: true,
        src: "EPSG:4326",
        version: '1.1.0',//wms version (ver get capabilities)
    }).addTo($var.Map.map);
    return wmsLayer;
}

