//Este js para la funcionalidad de los mapas bases del visor
var $var = require("../global/variables");//importar variables globales
var bbox = require("../../json/bbox.json");//json con array de dpto
//Mapa Base Gris
$var.Map.loadGris = L.gridLayer.googleMutant({
    type: 'roadmap',
    styles: $var.Map.basemapgris
});
//Mapa Base Satelital
$var.Map.loadSatelital = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoiam9uYXMxOTI3IiwiYSI6ImQwN2U2OThhZTZjYTczNjdlNGY4NGZiODBiOTQ1YjEzIn0.3vW7kgBYVw2fc4Ira6iS_A', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1Ijoiam9uYXMxOTI3IiwiYSI6ImQwN2U2OThhZTZjYTczNjdlNGY4NGZiODBiOTQ1YjEzIn0.3vW7kgBYVw2fc4Ira6iS_A'
});
//Mapa Base Hibrido
$var.Map.loadHibrido = L.gridLayer.googleMutant({
    type: 'hybrid'
});
//Mapa Base Noche 
$var.Map.loadNoche = L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', {
    attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
});
//Mapa Base OSM o default de google
$var.Map.loadOSM = L.gridLayer.googleMutant({
    type: 'roadmap'
});
//Mapa Base de terreno
$var.Map.loadTerreno = L.gridLayer.googleMutant({
    type: 'terrain'
});
// //Mapa Configuración Básica
$var.Map.mapOptions = {
    attributionControl: false,
    infoControl: true,
    layers: [$var.Map.loadOSM],
    center: [$var.Map.latInicial, $var.Map.lngInicial],
    zoom: $var.Map.zoomLevel,
    zoomControl: false
};
//Mapa Base - Configuración del servicio web de los mapas base
function layersPrint() {
    $.ajax({
        url: "https://geoportal.dane.gov.co/laboratorio/serviciosjson/visores/mapasbase.php?nivel_mapabase=1",
        type: "GET",
        success: function (data) {
            $.each(data.resultado, function (key, value) {
                var ul = $('.basemap__list');
                var liTemp = ul.find('li:first').clone(true);
                liTemp.addClass(value["CLASE"]).attr('id', value["CLASE"]).removeClass("--invisible");
                var liDivTemp = liTemp.children(".basemap__list__item__btn");
                liTemp.find(".basemap__list__item__btn__imageBox__image").attr('title', 'Geoportal DANE - ' + value["NOMBRE"]).attr('alt', 'Geoportal DANE - ' + value["NOMBRE"]).attr('src', value["IMAGEN_ENLACE"]);
                liDivTemp.find(".basemap__list__item__btn__name").text(value["NOMBRE"]);
                liTemp.appendTo(ul);
            });
            $('.basemap__gris').addClass('--active');
        }
    });
}
//Mapa base funcion - determina si hace click y coloca el mapa base por defecto este se lo asigna al ID del mapa base principal que encontraran en map.pug - #map__content__principalMap
$var.Map.mapasbase = function () {
    layersPrint();
    $var.Map.map = new L.Map("map__content__principalMap", $var.Map.mapOptions);
    L.control.zoom({
        position: 'topright',
        top: '50px'
    }).addTo($var.Map.map);
    //esacala grafica
    L.control.scale().addTo($var.Map.map);
    //grupo de mapas base
    var baseLayers = {
        "loadGris": $var.Map.loadGris,
        "loadSatelital": $var.Map.loadSatelital,
        "loadHibrido": $var.Map.loadHibrido,
        "loadNoche": $var.Map.loadNoche,
        "loadOSM": $var.Map.loadOSM,
        "loadTerreno": $var.Map.loadTerreno
    };
    //grupo de capas vectoriales
    var overlays = {
        "Vulnerabilidad": $var.Map.loadVulnerabilidad(),
        "resguardos": $var.Map.loadResguardos(),
        "pnn": $var.Map.loadParques(),
        "consejos": $var.Map.loadConsejos(),
        "campesinos": $var.Map.loadCampesinos(),
        "mgn": $var.Map.loadMgn2018()
    };
    //control de capas
    L.control.layers(baseLayers, overlays);
    //cuadro de coordenadas
    $('<div/>', {
        id: 'grid-coordenates'
    }).appendTo('.Geovisor__content');
    $('<div/>', {
        class: 'grid-container'
    }).appendTo('#grid-coordenates');
    $('<div/>', {
        class: 'grid-item scala',
        text: "Escala:",
    }).appendTo('.grid-container');
    $('<div/>', {
        class: 'grid-item scalaValor escala'
    }).appendTo('.grid-container');
    $('<div/>', {
        class: 'grid-item scala-down ',
        text: "Lat:",
    }).appendTo('.grid-container');
    $('<div/>', {
        class: 'grid-item scala-down latitud'
    }).appendTo('.grid-container');
    $('<div/>', {
        class: 'grid-item scala-down',
        text: "Long:",
    }).appendTo('.grid-container');
    $('<div/>', {
        class: 'grid-item scala-down longitud'
    }).appendTo('.grid-container');
    //evento para mostrar coordenadas
    $var.Map.map.on('mousemove click', function (e) {
        var coordenadas = e.latlng.toString();
        var res = coordenadas.replace("LatLng(", "");
        var res = res.replace(")", "");
        var res = res.split(", ");
        lat_string = degToDMS(res[0], 'LAT');
        lon_string = degToDMS(res[1], 'LON');

        $(".latitud").html(lat_string);
        $(".longitud").html(lon_string);
    });
    //conversio a grados minutos y segundos
    function degToDMS(decDeg, decDir) {
        /** @type {number} */
        var d = Math.abs(decDeg);
        /** @type {number} */
        var deg = Math.floor(d);
        d = d - deg;
        /** @type {number} */
        var min = Math.floor(d * 60);
        /** @type {number} */
        var sec = Math.floor((d - min / 60) * 60 * 60);
        if (sec === 60) { // can happen due to rounding above
            min++;
            sec = 0;
        }
        if (min === 60) { // can happen due to rounding above
            deg++;
            min = 0;
        }
        /** @type {string} */
        var min_string = min < 10 ? "0" + min : min;
        /** @type {string} */
        var sec_string = sec < 10 ? "0" + sec : sec;
        /** @type {string} */
        var dir = (decDir === 'LAT') ? (decDeg < 0 ? "S" : "N") : (decDeg < 0 ? "W" : "E");

        return (decDir === 'LAT') ?
            deg + "&deg;" + min_string + "&prime;" + sec_string + "&Prime;" + dir :
            deg + "&deg;" + min_string + "&prime;" + sec_string + "&Prime;" + dir;
    };
    let bboxTemp = bbox["05001"];
    $var.Map.map.fitBounds([
        [bboxTemp[0][1], bboxTemp[0][0]],
        [bboxTemp[1][1], bboxTemp[1][0]]
    ]);
    $("#filter__depto").val("05 - ANTIOQUIA");
    $("#filter__municipio").removeClass("--invisible");
    $var.Map.deptovalue = "05";
    $var.Map.loadMunicipios();
    setTimeout(function(){$("#filter__mpio").val("05001 - MEDELLÍN");}, 3000);
        

    // $var.Map.map.eachLayer(function(layer,i){
    //     console.log(layer)
    //     console.log(i)
    // });  
    // $var.Map.map.on('baselayerchange', function(e) {
    //     alert('Changed to ' + e.name);
    //   })        
}
